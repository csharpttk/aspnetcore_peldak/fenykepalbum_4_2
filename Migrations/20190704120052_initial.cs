﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace fenykepalbum_4_2.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Kepek",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Filenev = table.Column<string>(nullable: true),
                    Holkeszult = table.Column<string>(nullable: true),
                    Mikor = table.Column<DateTime>(nullable: false),
                    Leiras = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kepek", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Kepek",
                columns: new[] { "Id", "Filenev", "Holkeszult", "Leiras", "Mikor" },
                values: new object[] { 1, "WP_20150531_15_50_32_Pro.jpg", "Lednice", "Kastély park, tó", new DateTime(2015, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Kepek",
                columns: new[] { "Id", "Filenev", "Holkeszult", "Leiras", "Mikor" },
                values: new object[] { 2, "WP_20150531_16_01_58_Pro.jpg", "Lednice", "Kastély park, tó", new DateTime(2015, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Kepek");
        }
    }
}
